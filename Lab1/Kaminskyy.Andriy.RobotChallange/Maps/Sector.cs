﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Maps
{
	public class Sector
	{
		public List<Robot.Common.EnergyStation> _stations;
		public List<Robot.Common.Robot> _robots;
		public Position SectorCoords { get; }

		public Sector(int x, int y)
		{
			_stations = new List<EnergyStation>();
			_robots = new List<Robot.Common.Robot>();

			SectorCoords = new Position(x, y);
		}

		public void AddStation(Robot.Common.EnergyStation station)
		{
			_stations.Add(station);
		}

		public void AddRobot(Robot.Common.Robot robot)
		{
			_robots.Add(robot);
		}

		public void ClearRobots()
		{
			_robots.Clear();
		}
	}
}
