﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaminskyy.Andriy.RobotChallange.Maps
{
	public interface IMap
	{
		Cell this[int x, int y] { get; }
		void Build(Robot.Common.Map input);
	}
}
