﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Maps
{
	public class InterestMap : IMap
	{
		private Cell[,] _array;
		private Sector[,] _sectorArray;
		private const int StationValue = 2;

		private enum ParameterType
		{
			Value,
			Potential
		}

		public bool IsBuilt { get; private set; }

		public InterestMap()
		{
			IsBuilt = false;
			_array = new Cell[100, 100];
			_sectorArray = new Sector[5, 5];
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					_sectorArray[i, j] = new Sector(i, j);
				}
			}
			for (int i = 0; i < 100; i++)
			{
				for (int j = 0; j < 100; j++)
				{
					_array[i,j] = new Cell(i, j);
				}
			}
		}
		public Cell this[int x, int y] => _array[x, y];

		public Sector GetSector(int a, int b)
		{
			return _sectorArray[a, b];
		}


		public void Build(Map input)
		{
			if (!IsBuilt)
			{
				IsBuilt = true;
				foreach (var station in input.Stations)
				{
					AddPotentialToRadius(station.Position.X, station.Position.Y, StationValue);		//
					_array[station.Position.X, station.Position.Y].Potential++;						//	Build Cells
					_array[station.Position.X, station.Position.Y].IsStation = true;				//
					FindSector(station.Position.X, station.Position.Y).AddStation(station);	//Build sectors
				}
			}
		}

		private Sector FindSector(int x, int y)
		{
			return _sectorArray[x / 20, y / 20];
		}

		public void UpdateSectors(IList<Robot.Common.Robot> robots)
		{
			foreach (var sector in _sectorArray)
			{
				sector.ClearRobots();	
			}
			foreach (var robot in robots)
			{
				FindSector(robot.Position.X, robot.Position.Y).AddRobot(robot);
			}
		}

		#region ValueMap
		public void UpdateValueMap(Map input)
		{
			ClearValueMap();
			foreach (var station in input.Stations)
			{
				SetValueToRadius(station.Position.X, station.Position.Y, station.Energy);
			}
		}
		public void ClearValueMap()
		{
			foreach (var point in _array)
			{
				point.Value = 0;
			}
		}
		#endregion

		#region ChangeSomethingInRadius

		private void ChangeSomethingInRadius(int x, int y, int something, bool set, ParameterType type)
		{
			for (int i = x - 2; i <= x + 2; i++)
			{
				for (int j = y - 2; j <= y + 2; j++)
				{
					if (j >= 0 && j < 100 && i >= 0 && i < 100)
					{
						if (set)
						{
							if (type == ParameterType.Potential)
								_array[i, j].Potential = something;
							else if (type == ParameterType.Value)
								_array[i, j].Value = something;
						}
						else
						{
							if (type == ParameterType.Potential)
								_array[i, j].Potential += something;
							else if (type == ParameterType.Value)
								_array[i, j].Value += something;
						}
					}
				}
			}
		}

		private void AddPotentialToRadius(int x, int y, int pot)
		{
			ChangeSomethingInRadius(x, y, pot, false, ParameterType.Potential);
		}
		private void SetPotentialToRadius(int x, int y, int pot)
		{
			ChangeSomethingInRadius(x, y, pot, true, ParameterType.Potential);
		}
		private void SetValueToRadius(int x, int y, int val)
		{
			ChangeSomethingInRadius(x, y, val, true, ParameterType.Value);
		}

		public void AddHarvester(Position pos)
		{
			int x = pos.X;
			int y = pos.Y;
			if (!_array[x, y].IsHarvested)
			{
				_array[x, y].IsHarvested = true;
				for (int i = x - 2; i <= x + 2; i++) {
					for (int j = y - 2; j <= y + 2; j++) {
						if (j >= 0 && j < 100 && i >= 0 && i < 100)
						{
							if (_array[i, j].IsStation)
							{
								SetPotentialToRadius(i, j, 0);
							}
						}
					}
				}
			}
			else
			{
				throw new ArgumentException("This cell is already marked for harvest");
			}
		}

		#endregion
		
	}
}
