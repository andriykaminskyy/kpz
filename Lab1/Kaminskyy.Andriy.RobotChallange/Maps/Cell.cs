﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaminskyy.Andriy.RobotChallange.Maps
{
	public class Cell
	{
		public Cell(int x, int y)
		{
			Value = 0;
			Potential = 0;
			IsStation = false;
			Location = new Position(x, y);
		}
		public Position Location { get; private set; } 
		public int Potential { get; set; }
		public int Value { get; set; }
		public bool IsStation { get; set; }

		public bool IsHarvested { get; set; }


	}
}
