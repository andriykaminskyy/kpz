﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kaminskyy.Andriy.RobotChallange.Loggers;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Kaminskyy.Andriy.RobotChallange.Robots;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange
{
    class KamisnskyyAlgorythm : IRobotAlgorithm
	{
		private InterestMap _myMap;
		private int _currentRound;
		private IRobotManager _robotManager;

		public KamisnskyyAlgorythm()
		{
			_myMap = new InterestMap();
			_robotManager = new RobotManager();
			_currentRound = 0;
			Logger.OnLogRound += LoggerOnOnLogRound;
		}

		private void LoggerOnOnLogRound(object sender, LogRoundEventArgs logRoundEventArgs)
		{
			_currentRound++;
		}


		public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {																																																																								
			if (!_myMap.IsBuilt)	//First move
			{
				_myMap.Build(map);
				_robotManager.BuildInitialRobots(robots, _myMap, this);
			}
			_myMap.UpdateSectors(robots);

			IRobot current = _robotManager.GetRobot(robotToMoveIndex);
	        return current.DoStep(robots[robotToMoveIndex], map, _currentRound);
        }

        public string Author => "Andriy Kaminskyy";

        public string Description => " ";
    }
}
