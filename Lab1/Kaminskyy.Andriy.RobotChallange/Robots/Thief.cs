﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Robots
{
	class Thief : IRobot
	{
		private InterestMap _myMap;
		private IRobotManager _myManager;

		private Position _sectorGoal;

		public Thief(InterestMap map, IRobotManager manager)
		{
			_myMap = map;
			_myManager = manager;
		}
		public RobotCommand DoStep(Robot.Common.Robot me, Map basicMap, int roundNumber)
		{
			if (_sectorGoal == null)
			{
				FindTopPlayer();
			}
			
			if (IsWithinGoal(me))
			{
				return Hunt(me, roundNumber);
			}
			return GenerateMoveTo(me, new Position(_sectorGoal.X * 20 + 10, _sectorGoal.Y * 20 + 10));

		}

		private RobotCommand Hunt(Robot.Common.Robot me, int roundNumber)
		{
			var targets = _myMap.GetSector(_sectorGoal.X, _sectorGoal.Y)._robots
				.Where(robot => robot.Owner.Name != "Andriy Kaminskyy")
				.OrderByDescending(robot => (float)robot.Energy * 0.1 - DistanceHelper.GetTravelPrice(me.Position, robot.Position));
			if (!targets.Any())
			{
				_sectorGoal = null;
				return new CollectEnergyCommand();
			}
			foreach (var target in targets) {
				if ((float)target.Energy * 0.1 - DistanceHelper.GetTravelPrice(me.Position, target.Position) - 30 >= 0 &&
					DistanceHelper.GetTravelPrice(me.Position, target.Position) + 30 < me.Energy)
					return new MoveCommand() {NewPosition = target.Position};
			}
			return new CollectEnergyCommand();
		}

		private bool IsWithinGoal(Robot.Common.Robot me)
		{
			if (me.Position.X < _sectorGoal.X * 20 ||
				me.Position.X > (_sectorGoal.X + 1) * 20 ||
				me.Position.Y < _sectorGoal.Y * 20 ||
				me.Position.Y > (_sectorGoal.Y + 1) * 20)
			{
				return false;
			}
			return true;
		}
		private RobotCommand GenerateMoveTo(Robot.Common.Robot me, Position goal)
		{
			Position concreteGoal = goal;
			int basicCost = DistanceHelper.GetTravelPrice(me.Position, concreteGoal);
			int currentCost = basicCost;
			var nextPosition = concreteGoal.Copy();
			int divisor = 1;
			while (currentCost * divisor > me.Energy + 70)
			{
				divisor++;
				nextPosition.X = (concreteGoal.X - me.Position.X) / divisor + me.Position.X;
				nextPosition.Y = (concreteGoal.Y - me.Position.Y) / divisor + me.Position.Y;
				currentCost = DistanceHelper.GetTravelPrice(me.Position, nextPosition);
			}
			return new MoveCommand() { NewPosition = nextPosition };
		}

		private void FindTopPlayer()
		{
			Dictionary<Robot.Common.Owner, int> players = new Dictionary<Owner, int>();
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					var sector = _myMap.GetSector(i, j);
					foreach (var robot in sector._robots)
					{
						if (!players.ContainsKey(robot.Owner))
							players.Add(robot.Owner, robot.Energy);
						else
							players[robot.Owner] += robot.Energy;
					}
					
				}
			}
			var topPlayer = players.Where(pair => pair.Key.Name != "Andriy Kaminskyy").OrderByDescending(pair => pair.Value).First().Key;

			Dictionary<Sector, int> topSectors = new Dictionary<Sector, int>();
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					var sector = _myMap.GetSector(i, j);
					topSectors.Add(sector, 0);
					foreach (var robot in sector._robots)
					{
						if (robot.Owner == topPlayer)
						{
							topSectors[sector] += robot.Energy;
						}
					}

				}
			}

			_sectorGoal = topSectors.OrderByDescending(pair => pair.Value).First().Key.SectorCoords;
		}
		
	}
}
