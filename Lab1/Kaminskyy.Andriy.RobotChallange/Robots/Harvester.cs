﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kaminskyy.Andriy.RobotChallange.Loggers;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Robots
{
	public class Harvester : IRobot
	{
		private InterestMap _mainMap;
		private IRobotManager _myManager;

		private Position _goal;
		public Harvester(InterestMap map, IRobotManager manager)
		{
			_mainMap = map;
			_myManager = manager;
		}

		public RobotCommand DoStep(Robot.Common.Robot me, Map basicMap, int roundNumber)
		{
			if (me.Energy == 0)
			{
				return new CollectEnergyCommand();
			}
			if (_goal == null)
			{
				FindNewGoal(me.Position, me);
			}
			if (me.Position == _goal)
			{
				if (roundNumber < 20 && me.Energy > 410)
				{
					_myManager.AddNewRobot(new Harvester(_mainMap, _myManager));
					return new CreateNewRobotCommand() {NewRobotEnergy = 200};
				}
				if (roundNumber >= 30 && me.Energy > 600)
				{
					if (_myManager.CanAddThief())
					{
						_myManager.AddNewRobot(new Thief(_mainMap, _myManager));
						return new CreateNewRobotCommand() {NewRobotEnergy = 400};
					}
				}

				return new CollectEnergyCommand();
			}
			return GenerateMove(me);
		}

		private RobotCommand GenerateMove(Robot.Common.Robot me)
		{
			int basicCost = DistanceHelper.GetTravelPrice(me.Position, _goal);
			int currentCost = basicCost;
			var nextPosition = _goal.Copy();
			int divisor = 1;
			while (currentCost * divisor > me.Energy)
			{
				divisor++;
				nextPosition.X = (_goal.X - me.Position.X) / divisor + me.Position.X;
				nextPosition.Y = (_goal.Y - me.Position.Y) / divisor + me.Position.Y;
				currentCost = DistanceHelper.GetTravelPrice(me.Position, nextPosition);
			}
			return new MoveCommand() {NewPosition = nextPosition };
		}

		private void FindNewGoal(Position myPos, Robot.Common.Robot me)
		{
			int distance = CalculateMaxDistance(me.Energy);

			while (distance < 200)
			{
				List<Position> closePositions = DistanceHelper.GetPositionsInRadius(myPos, distance);
				List <Cell> candidates = new List<Cell>();
				foreach (var pos in closePositions)
				{
					candidates.Add(_mainMap[pos.X, pos.Y]);
				}
				var possibleGoal = candidates.Where(cell => cell.Potential > 0 && !cell.IsHarvested).OrderBy(cell => cell.Potential);
				if (possibleGoal.Any())
				{
					_goal = possibleGoal.Last().Location;
					_mainMap.AddHarvester(_goal);

					return;
				}
				distance++;
			}
			throw new Exception();


		}

		private int CalculateMaxDistance(int energy, int steps = 4)
		{
			return (int) Math.Sqrt(energy / steps);
		}
	}
}
