﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Robots
{
	public interface IRobotManager
	{
		IRobot GetRobot(int id);
		void AddNewRobot(IRobot robot);

		bool IsBuilt { get; }

		void BuildInitialRobots(IList<Robot.Common.Robot> robots, InterestMap map, IRobotAlgorithm parentAlgorithm);
		bool CanAddThief();
	}
}
