﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Robots
{
	public class RobotManager : IRobotManager
	{
		private readonly IDictionary<int, IRobot> _robotsAndIds;
		private readonly Queue<IRobot> _futureRobots;

		private bool _hasThief;

		public bool IsBuilt { get; private set; }

		public RobotManager()
		{
			_hasThief = false;
			_robotsAndIds = new Dictionary<int, IRobot>();
			_futureRobots = new Queue<IRobot>();
			IsBuilt = false;
		}
		public IRobot GetRobot(int id)
		{
			if (_robotsAndIds.ContainsKey(id))
			{
				return _robotsAndIds[id];
			}
			if (_futureRobots.Count > 0)
			{
				_robotsAndIds.Add(id, _futureRobots.Dequeue());
				return _robotsAndIds[id];
			}
			throw new ArgumentException("Robot with this id does not exist and the queue is empty.");
		}

		public void AddNewRobot(IRobot robot)
		{
			_futureRobots.Enqueue(robot);
		}

		public void BuildInitialRobots(IList<Robot.Common.Robot> robots, InterestMap map, IRobotAlgorithm parentAlgorithm)
		{
			if (!IsBuilt)
			{
				IsBuilt = true;
				for (int i = 0; i < robots.Count; i++)
				{
					if (robots[i].Owner.Algorithm.Equals(parentAlgorithm) && !_robotsAndIds.ContainsKey(i))
					{
						_robotsAndIds.Add(i, new Harvester(map, this));
					}
				}
			}
		}

		public bool CanAddThief()
		{
			if (!_hasThief)
			{
				_hasThief = true;
				return true;
			}
			return false;
		}
	}
}
