﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Robots
{
	public interface IRobot
	{
		RobotCommand DoStep(Robot.Common.Robot me, Robot.Common.Map basicMap, int roundNumber);
	}
}
