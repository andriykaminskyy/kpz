﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Kaminskyy.Andriy.RobotChallange.Maps;

namespace Kaminskyy.Andriy.RobotChallange.Loggers
{
	public static class FileLogger
	{
		private static readonly FileStream _file;
		static FileLogger()
		{
			_file = new FileStream(@"game.log", FileMode.Create);
		}
		public static void Log(string input)
		{
			byte[] array = System.Text.Encoding.Default.GetBytes(input+'\n');
			_file.Write(array, 0, array.Length);
		}

		public static void Log(IMap map)
		{
			StringBuilder log = new StringBuilder();
			for (int j = 0; j < 100; j++)
			{
				for (int i = 0; i < 100; i++)
				{
					log.Append(map[i, j].IsStation ? "x" : map[i, j].Potential.ToString());
				}
				log.Append('\n');
			}
			byte[] array = System.Text.Encoding.Default.GetBytes(log.ToString());
			_file.Write(array, 0, array.Length);
		}
	}
}
