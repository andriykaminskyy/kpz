﻿using Kaminskyy.Andriy.RobotChallange.Maps;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace Kaminskyy.Andriy.RobotChallange
{
	public static class DistanceHelper
	{
		public static List<Position> GetPositionsInRadius(Position point, int radius)
		{
			List<Position> result = new List<Position>();
			for (int i = 1; i <= radius; i++)
			{
				for (int j = 0; j <= i; j++)
				{
					int xoffset = j;
					int yoffset = i - j;

					TryAddCell(point.X + xoffset, point.Y + yoffset, result);
					
					TryAddCell(point.X - xoffset, point.Y - yoffset, result);
					if (xoffset != 0 && yoffset != 0)
					{
						TryAddCell(point.X + xoffset, point.Y - yoffset, result);
					
						TryAddCell(point.X - xoffset, point.Y + yoffset, result);
					}
				}
			}
			return result;
		}

		private static void TryAddCell(int x, int y, List<Position> list)
		{
			if (x >= 0 && x < 100 && y >= 0 && y < 100)
			{
				list.Add(new Position(x, y));
			}
				
		}

		public static int GetTravelPrice(Position a, Position b)
		{
			return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
		}

	}
}
