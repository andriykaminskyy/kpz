﻿using NUnit.Framework;
using Kaminskyy.Andriy.RobotChallange.Robots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Moq;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Tests
{
	[TestFixture()]
	public class RobotManagerTests
	{
		[Test()]
		public void GetRobot_NoRobots_ThrowsException()
		{
			//Arrange
			var manager = new RobotManager();

			//Act & Assert

			Assert.That(() => manager.GetRobot(2), Throws.Exception);
		}

		[Test()]
		public void GetRobot_AddRobotToQueue_ReturnsRobot()
		{
			//Arrange
			var manager = new RobotManager();
			var robotMock = new Mock<IRobot>().Object;
			manager.AddNewRobot(robotMock);

			//Assert
			Assert.That(manager.GetRobot(2), Is.EqualTo(robotMock));
			Assert.That(manager.GetRobot(2), Is.EqualTo(robotMock));
		}

		[Test()]
		public void BuildInitialRobots_SomeRobots_BuildsCorrectly()
		{
			//Arrange

			var manager = new RobotManager();

			var me = new Owner() {Algorithm = new Mock<IRobotAlgorithm>().Object};
			var notMe = new Owner() { Algorithm = new Mock<IRobotAlgorithm>().Object };

			var one = new Robot.Common.Robot() { Owner = me };
			var two = new Robot.Common.Robot() { Owner = me };
			var three = new Robot.Common.Robot() { Owner = me };

			var list = new List<Robot.Common.Robot>
			{
				one,
				new Robot.Common.Robot() {Owner = notMe},
				two,
				new Robot.Common.Robot() {Owner = notMe},
				three,
				new Robot.Common.Robot() {Owner = notMe}
			};


			//Act 
			
			manager.BuildInitialRobots(list, new InterestMap(), me.Algorithm);

			//Assert

			Assert.That(manager.GetRobot(0), Is.InstanceOf<IRobot>());
			Assert.That(() => manager.GetRobot(1), Throws.ArgumentException);
			Assert.That(manager.GetRobot(2), Is.InstanceOf<IRobot>());
			Assert.That(() => manager.GetRobot(3), Throws.ArgumentException);
			Assert.That(manager.GetRobot(4), Is.InstanceOf<IRobot>());
			Assert.That(() => manager.GetRobot(5), Throws.ArgumentException);
		}
	}
}