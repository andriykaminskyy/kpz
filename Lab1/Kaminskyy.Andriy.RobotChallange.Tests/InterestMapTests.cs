﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaminskyy.Andriy.RobotChallange.Maps;
using Moq;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Tests
{
	[TestFixture]
	public class InterestMapTests
	{
		[Test]
		public void Build_MapWithOneStation_CorrectResult()
		{
			//Arrange
			var stations = new List<Robot.Common.EnergyStation>();
			var stationPosition = new Position(5, 5);
			var stationOne = new Robot.Common.EnergyStation() {Position = stationPosition};
			stations.Add(stationOne);

			var mapMock = new Robot.Common.Map() {Stations = stations};

			InterestMap mainMap = new InterestMap();

			//Act
			mainMap.Build(mapMock);

			//Assert
			for (int i = 0; i < 100; i++)
			{
				for (int j = 0; j < 100; j++)
				{
					if (i == stationPosition.X && j == stationPosition.Y)
						Assert.That(mainMap[i, j].IsStation);
					if (i >= stationPosition.X - 2 && i <= stationPosition.X + 2 &&
						j >= stationPosition.Y - 2 && j <= stationPosition.Y + 2)
						Assert.That(mainMap[i, j].Potential, Is.GreaterThanOrEqualTo(1));
					else
						Assert.That(mainMap[i, j].Potential, Is.EqualTo(0));

				}
			}
		}

		[Test]
		public void AddHarvester_ThreeStations_LowersPotentialOfTwo()
		{
			//Arrange
			var stationPositionOne = new Position(6, 5);
			var stationPositionTwo = new Position(8, 5);
			var stationPositionThree = new Position(50, 50);

			var stations = new List<Robot.Common.EnergyStation>();
			stations.Add(new Robot.Common.EnergyStation() { Position = stationPositionOne });
			stations.Add(new Robot.Common.EnergyStation() { Position = stationPositionTwo });
			stations.Add(new Robot.Common.EnergyStation() { Position = stationPositionThree });

			var mapMock = new Robot.Common.Map() { Stations = stations };

			InterestMap mainMap = new InterestMap();
			mainMap.Build(mapMock);

			//Act
			mainMap.AddHarvester(new Position(7, 5));
			

			//Assert
			for (int i = 0; i < 100; i++)
			{
				for (int j = 0; j < 100; j++)
				{
					if (mainMap[i, j].Potential == 1)
						TestContext.Out.WriteLine($"<{i}, {j}> = {mainMap[i, j].Potential}");
					if (i >= stationPositionThree.X - 2 && i <= stationPositionThree.X + 2 &&
						j >= stationPositionThree.Y - 2 && j <= stationPositionThree.Y + 2)
						Assert.That(mainMap[i, j].Potential, Is.GreaterThanOrEqualTo(1));
					else
						Assert.That(mainMap[i, j].Potential, Is.EqualTo(0));

				}
			}
			
		}

		[Test]
		public void AddHarvester_TwiceOnSameSpot_ThrowsException()
		{
			//Arrange
			InterestMap mainMap = new InterestMap();
			//Act
			mainMap.AddHarvester(new Position(7, 5));
			//Assert
			Assert.That(() => mainMap.AddHarvester(new Position(7, 5)), Throws.ArgumentException);
		}
	}
}
