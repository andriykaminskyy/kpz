﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Robot.Common;

namespace Kaminskyy.Andriy.RobotChallange.Tests
{
	[TestFixture(TestOf = typeof(DistanceHelper))]
	public class DistanceHelperTests
	{//Method_State_Result
		  [Test]
		public void GetTravelPrice_ZeroOneAndTwoTwo_EqualsFive()
		{
			int expected = 5;
			int actual = DistanceHelper.GetTravelPrice(new Position(0, 1), new Position(2, 2));

			Assert.That(actual, Is.EqualTo(expected));
		}
		[Test]
		public void GetTravelPrice_SamePoints_EqualsZero()
		{
			int expected = 0;
			int actual = DistanceHelper.GetTravelPrice(new Position(1, 1), new Position(1, 1));

			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void GetPositionsInRadius_RadiusIsThree_ResultsAreWithinRadius()
		{
			//Arrange
			var initialPosition = new Position(50, 50);
			var radius = 3;

			//Act
			var positions = DistanceHelper.GetPositionsInRadius(initialPosition, radius);
			
			//Assert
			foreach (var pos in positions)
			{
				Assert.That(pos.X - initialPosition.X + pos.Y - initialPosition.Y, 
					Is.LessThanOrEqualTo(radius));	
			}
		}

		[Test]
		public void GetPositionsInRadius_RadiusIsThree_CorrectNumberOfPositions()
		{
			//Arrange
			var initialPosition = new Position(50, 50);
			var radius = 3;
			var expectedCount = 0;
			for (int i = 1; i <= radius; i++)
			{
				expectedCount += i * 4;
			}

			//Act
			var positions = DistanceHelper.GetPositionsInRadius(initialPosition, radius);

			//Assert
			Assert.That(positions, Has.Count.EqualTo(expectedCount));
		}
	}
}
